# Codementor: Let’s Build a Redux Powered React Application
This is an implementation of a tutorial by Robin Orheden on Stormpath, the tutorial is freely avaliable at: https://stormpath.com/blog/build-a-redux-powered-react-application, in this tutorial a simple app is created using redux and react, this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)
- npm available when installing [Node.js](https://nodejs.org/en/)

## Installing

just clone this repository and run npm install by using these commands:

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Redux_React_Stormpath.git
cd Redux_React_Stormpath/src
npm install
```

## Running
This example uses react scripts and can be ran by typing this command
```
cd src/
npm start
```

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source and tutorial by Robin Orheden available at: https://stormpath.com/blog/build-a-redux-powered-react-application
